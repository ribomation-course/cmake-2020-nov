#include "gtest/gtest.h"
#include "fibonacci.hxx"

TEST(fib, simple) {
    ASSERT_EQ(fibonacci(10), 55);
}

TEST(fib, moderate) {
    ASSERT_EQ(fibonacci(42), 267914296);
}

