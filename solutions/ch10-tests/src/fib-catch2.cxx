#define CATCH_CONFIG_MAIN   //once per executable
#include "catch.hpp"
#include "fibonacci.hxx"

SCENARIO("testing fibonacci()", "[base]") {
    GIVEN("a lousy function") {
        WHEN("the arg is 10") {
            THEN("it should return 55") {
                REQUIRE(fibonacci(10) == 55);
            }
        }
		
		WHEN("the arg is 42") {
            THEN("it should return 267914296") {
                REQUIRE(fibonacci(42) == 267914296);
            }
        }
    }
}

