# Build

    mkdir bld && cd bld
    cmake -G Ninja ..
    cmake --build .

# Run Test

    cd bld
    ctest

# Run All

    cd bld
    cmake --build . --target run


