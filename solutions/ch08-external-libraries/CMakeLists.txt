cmake_minimum_required(VERSION 3.12)
project(circular-buffer 
	VERSION		1
    LANGUAGES	CXX
)

set(CMAKE_CXX_STANDARD			17)
set(CMAKE_CXX_STANDARD_REQUIRED	ON)
set(CMAKE_CXX_EXTENSIONS 		OFF)
set(CMAKE_BUILD_TYPE			Release)

set(Boost_USE_STATIC_LIB	YES)
find_package(Boost REQUIRED 
	COMPONENTS container
)
if(NOT Boost_FOUND)
	message(FATAL_ERROR "Failed to load Boost")
endif()

add_executable(app src/app.cxx)
target_link_libraries(app
	PRIVATE	Boost::container
)
target_include_directories(app
	PRIVATE	${BOOST_INCLUDE_DIRS}
)


add_custom_target(run
	COMMAND $<TARGET_FILE:app> 10
)
