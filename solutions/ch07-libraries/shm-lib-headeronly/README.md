# Build & Run

    mkdir bld && cd bld
    cmake -G Ninja ..
    cmake --build .
    cmake --build . --target run

