# Build & Run

    mkdir bld && cd $_
    rm -rf * .ninja*
    cmake -G Ninja ..
    cmake --build .
    cmake --build . --target run

