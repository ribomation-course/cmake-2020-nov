cmake_minimum_required(VERSION 3.10)
project(shm-app 
	VERSION			2
	LANGUAGES 		CXX
	DESCRIPTION		"Computes the N first Fibonacci numbers and \
					 stores them in a shared memory segment, where the \
					 parent process, waits until child termination and \
					 then reads/prints the results"
)

set(CMAKE_CXX_STANDARD			17)
set(CMAKE_CXX_STANDARD_REQUIRED	ON)
set(CMAKE_CXX_EXTENSIONS 		OFF)

add_executable(shm-app )
target_sources(shm-app PRIVATE
    src/cxx/app.cxx
)
target_sources(shm-app PRIVATE
	src/cxx/shared-memory.cxx 
    src/hdrs/shared-memory.hxx 
)
target_include_directories(shm-app PRIVATE 
	src/hdrs
)
target_compile_options(shm-app PRIVATE 
	-Wall -Wextra -Wfatal-errors -Wno-pointer-arith
)
target_compile_definitions(shm-app PRIVATE
	NDEBUG=1   
)
target_link_libraries(shm-app PRIVATE 
	rt
)

add_custom_target(run
	COMMAND $<TARGET_FILE:shm-app> 42
)
