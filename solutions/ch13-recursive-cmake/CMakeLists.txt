cmake_minimum_required(VERSION 3.12)
project(yaml-proj
  LANGUAGES		CXX
  VERSION		1.42
  DESCRIPTION	"Sample non-trivial project"
  HOMEPAGE_URL	https://www.ribomation.se/courses/build/cmake
)
enable_testing()

set(CMAKE_CXX_STANDARD 		      17)
set(CMAKE_CXX_STANDARD_REQUIRED	YES)
set(CMAKE_CXX_EXTENSIONS 		    NO)
set(CMAKE_BUILD_TYPE            Release)

add_subdirectory(dependencies)
add_subdirectory(main)

add_custom_target(run
    COMMAND ${CMAKE_BINARY_DIR}/yaml-app
)
