#include <iostream>
#include <vector>
#include <map>
#include <string>
#include "yaml-codec.hxx"
using namespace std;
using namespace std::literals;

int main() {
    vector<string> names = {
        "Anna", "Berit", "Carin", "Doris"
    };
    map<string,string> person = {
        {"name", "Anna Conda"},
        {"age" , "42"}
    };

    YamlCodec codec;
    cout << "names:\n"  << codec.toYaml(names)  << endl;
    cout << "person:\n" << codec.toYaml(person) << endl;
    
    return 0;
}
