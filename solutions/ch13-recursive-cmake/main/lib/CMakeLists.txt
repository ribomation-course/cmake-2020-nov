
add_library(codec STATIC "")
target_sources(codec PRIVATE
    impl/yaml-codec.cxx
    hdrs/yaml-codec.hxx
)
target_include_directories(codec PUBLIC
    hdrs
    ${YAMLCPP_INCLUDE_DIR}
)
target_link_libraries(codec PUBLIC
    ${YAMLCPP_LIBFILE}
)

