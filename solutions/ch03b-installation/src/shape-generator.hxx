#pragma once
#include <vector>
#include "shape.hxx"
#include "rect.hxx"
#include "circle.hxx"
#include "triangle.hxx"
#include "square.hxx"

struct ShapeGenerator {
    Shape* mkShape();
    std::vector<Shape*> mkShapes(int numShapes);
};
