#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "yaml-cpp/yaml.h"
using namespace std;

int main(int argc, char** argv) {
    vector<string> tags = {"foo", "bar", "fee"};
    map<string, string> person = {
        {"name" , "Anna Conda"},
        {"age"  , "42"},
        {"color", "red"}
    };

    cout << "Sample YaML document:\n";
    YAML::Emitter yaml{cout};
	yaml << YAML::BeginMap 
            << YAML::Key << "person" << YAML::Value << person 
			<< YAML::Key << "tags"   << YAML::Value << tags 
		 << YAML::EndMap;
    cout << "\n---END---\n";

	return 0;
}
