#!/usr/bin/env bash
set -eu

CC=g++
AR=ar

echo '--- Init ---'
CXX_FLAGS="-std=c++17 -Wall -Wextra -Werror -Wfatal-errors"
(set -x;
rm -rf bld
mkdir -p bld/objs bld/lib
)

echo '--- Compilation ---'
for file in `ls src/*.cxx`; do
	(set -x
	name=$(basename $file .cxx)
	[ $name = "shapes-app" ] && continue
	${CC} ${CXX_FLAGS} -c $file -o bld/objs/$name.o
	)
	echo '----'
done

echo '--- Archive ---'
(set -x
${AR} crs bld/lib/shapes.a bld/objs/*.o
)

echo '--- Linking ---'
(set -x
${CC} ${CXX_FLAGS} -c src/shapes-app.cxx -o bld/shapes-app.o
${CC} bld/shapes-app.o bld/lib/shapes.a -o bld/shapes-app
)
echo '--- Project Content ---'
tree

echo '--- Execution ---'
./bld/shapes-app
