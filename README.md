# CMake
### 26 - 27 November 2020 (*remote course*)
### 14 - 15 December 2020 (*classroom course*)

Welcome to this course that will get you up to speed with using CMake for C/C++ application build.


# Links
* [Installation instructions](./installation-instructions.md)
* [Zoom Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)  (*not applicable for classroom course*)
* [Course Details](https://www.ribomation.se/courses/build-tools/cmake)


# Course GIT Repo
It's recommended that you keep the git repo and your solutions separated. 
Create a dedicated directory for this course and a sub-directory for 
each chapter. Get the course repo initially by a `git clone` operation

    mkdir -p ~/cmake-course/my-solutions
    cd ~/cmake-course
    git clone <git HTTPS clone link> gitlab

During the course, solutions will be push:ed to this repo and you can get these by
a `git pull` operation

    cd ~/cmake-course/gitlab
    git pull


# Interesting Videos

* [CppCon 2016: Jason Turner “Rich Code for Tiny Computers: A Simple Commodore 64 Game in C++17”](https://youtu.be/zBkNBP00wJE)
* [Hello World from Scratch - Peter Bindels & Simon Brand [ACCU 2019]](https://youtu.be/MZo7k_IOCe8)
* [CppCon 2018: Michael Caisse “Modern C++ in Embedded Systems - The Saga Continues”](https://youtu.be/LfRLQ7IChtg)


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>



