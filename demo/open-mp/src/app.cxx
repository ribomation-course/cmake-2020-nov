#include <iostream>
#include <string>
#include <chrono>
#include <omp.h>
using namespace std;
using namespace std::chrono;

int main(int argc, char *argv[]) {
	auto n = argc > 1 ? stol(argv[1]) : 1'000'000'000;
	cout << "# CPUs   : " << omp_get_num_procs() << endl;
	cout << "# threads: " << omp_get_max_threads() << endl;
	cout << "SUM(1 .. " << n << ") ..." << endl;
	
	auto startTime = high_resolution_clock::now();
	
	auto sum       = 0ULL;
#pragma omp parallel for reduction(+ : sum)
	for (auto k = 1; k <= n; k++) { sum += k; }
	
	auto elapsed = high_resolution_clock::now() - startTime;
	auto ms      = duration_cast<milliseconds>(elapsed).count();
	
	cout << "RESULT : " << sum << endl;
	cout << "Elapsed: " << ms * 1E-3 << " seconds" << endl;

  return 0;
}


