#include <iostream>
#include <string>
#include "numbers.hxx"
using namespace std;

int main(int argc, char** argv) {
    auto n = (argc == 1) ? 42U : stoi(argv[1]);
    auto f = Numbers::fibonacci(n);
    cout << "fib(" << n << ") = " << f << endl;
    return 0;
}

