
#include <iostream>
#include <boost/program_options.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

using namespace std;
namespace opts = boost::program_options;
namespace uuid = boost::uuids;


int main(int argc, char** argv) {
	opts::options_description desc("Options");
	desc.add_options()
        ("help,h", "display this help")
        ("add,a", opts::value< string >(), "add a thingy")
        ;

	opts::variables_map vm;
    try {
        opts::store(opts::parse_command_line(argc, argv, desc), vm);
        opts::notify(vm);
    } catch (opts::error& error) {
        cerr << "Error: " << error.what() << "\n" << endl;
        return 1;
    }

	if (vm.count("add")) {
		cout << "ADD a thingy: " << vm["add"].as<string>() << endl;
	}
	
	uuid::random_generator gen;
	auto id = gen();
	cout << "ID: " << id << endl;
	
	return 0;
}

