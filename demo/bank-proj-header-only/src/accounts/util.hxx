#pragma once
#include <string>
#include <algorithm>
#include <cctype>
#include "util.hxx"
using namespace std;

string toUpperCase(string s) {
	transform(s.begin(), s.end(), s.begin(), [](auto ch){
		return ::toupper(ch);		
	});
	return s;
}
