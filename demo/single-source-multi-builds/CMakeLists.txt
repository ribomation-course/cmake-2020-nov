cmake_minimum_required(VERSION 3.10)
project(single-source-multi-builds
	VERSION		1
    LANGUAGES	CXX
)

set(CMAKE_CXX_STANDARD          17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS        OFF)

set(SRC_DIR 
    ${CMAKE_SOURCE_DIR}/src
)
set(SRC_COMMON 
    ${SRC_DIR}/variant-app.cxx
)

add_subdirectory(variant-first)
add_subdirectory(variant-second)
add_subdirectory(variant-third)

add_custom_target(run
    COMMAND ${CMAKE_BINARY_DIR}/variant-first/app
    COMMAND ${CMAKE_BINARY_DIR}/variant-second/app
    COMMAND ${CMAKE_BINARY_DIR}/variant-third/app
)

