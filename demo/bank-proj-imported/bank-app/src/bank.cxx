#include <iostream>
#include "account.hxx"
using namespace std;

int main() {
	Account acc{150};
	cout << "acc: " << acc.getBalance() << endl;
	acc.update(250);
	cout << "acc: " << acc.getBalance() << endl;
	return 0;
}
