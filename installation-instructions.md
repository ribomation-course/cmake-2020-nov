# Installation Instructions

In order to participate and perform the programming exercises of the course, 
you need to have the following installed.

* Zoom Client (*not applicable for classroom course*)
  - https://us02web.zoom.us/download
  - [Video/Audio Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)
* GIT Client
  - https://git-scm.com/downloads
* A (Ubuntu) Linux system
* C/C++ compiler
* CMake, Make, Ninja


Installation of Ubuntu Linux
----

We will do the exercises on Linux. Therefore you need access to a Linux or Unix system, preferably Ubuntu.

### Windows 10

If you are using Windows 10; install Ubuntu 20.04 @ WSL. Read how to do it here:
[Windows 10: WSL Installation Guide](https://docs.microsoft.com/en-us/windows/wsl/install-win10)

### Earlier version of Windows

If you are using an earlier version of Windows; install VirtualBox, create a
virtual machine and install the latest version of Ubuntu Desktop to the VM.
1. Install VirtualBox (VBox)<br/>
    <https://www.virtualbox.org/wiki/Downloads>
1. Create a new virtual machine (VM) i VBox, for Ubuntu Linux<br/>
    <https://www.virtualbox.org/manual/ch03.html>
1. Download an ISO file for the latest version of Ubuntu Desktop<br/>
    <http://www.ubuntu.com/download/desktop>
1. Mount the ISO file in the virtual CD drive of your VM
1. Start the VM and run the Ubuntu installation program.
    Ensure you install to the (virtual) hard-drive.
    Set a username and password when asked to and write them down.
1. Install the VBox guest additions<br/>
    <https://www.virtualbox.org/manual/ch04.html>


Installation within *NIX
----
Install the latest version of CMake, by downloading the `*.tar.gz` file, unpack it and follow the instructions in Readme.txt.
* https://cmake.org/download/

In addition; you need to install (_the commands below are for Ubuntu_)
* The Make and Ninja builder tools; `sudo apt install make ninja-build`
* The GNU C/C++ compiler, version 8 or later; `sudo apt install gcc-9 g++-9`
* The CLang C++ compiler, version 10 or later; `sudo apt install clang`
* Git client; `sudo apt install git`
* Any text editor you like, such as `nano`, `emacs`, `vim`


